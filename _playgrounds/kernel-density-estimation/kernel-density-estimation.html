---
title: "Kernel density estimation"
description: "Kernel density estimation is used to estimate the unknown density function in probability theory."
date: 2020-04-26
tags: Kernel density estimation
references:
  - author: "Murray Rosenblatt"
    title: "Remarks on Some Nonparametric Estimates of a Density Function"
    date: 1956
    link: https://projecteuclid.org/euclid.aoms/1177728190
  - author: "Emanuel Parzen"
    title: "On Estimation of a Probability Density Function and Mode"
    date: 1962
    link: https://projecteuclid.org/euclid.aoms/1177704472
  - author: "Bernard Silverman"
    title: "Density Estimation for Statistics and Data Analysis"
    date: 1986
    link: https://archive.org/details/densityestimatio00silv_0/page/45/mode/2up
  - author: "Wikipedia"
    title: "Kernel density estimation"
    date: July 2020
    link: https://en.wikipedia.org/wiki/Kernel_density_estimation
  - author: "Wikipedia"
    title: "Multivariate kernel density estimation"
    date: July 2020
    link: https://en.wikipedia.org/wiki/Multivariate_kernel_density_estimation
  
head_additional: >
  <style>
    #btn-kde-run-stop.btn-secondary .kde-run {display: inherit;}
    #btn-kde-run-stop.btn-secondary .kde-stop {display: none;}
    #btn-kde-run-stop.btn-warning .kde-run {display: none;}
    #btn-kde-run-stop.btn-warning .kde-stop {display: inherit;}
  </style>
foot_additional: >
  {% include mathjax.html %}
  {% include svgjs.html %}
  <script src="{{ 'kernel-density-estimation.js' | prepend: page.url | absolute_url }}"></script>
---

<div class="row">
  <div class="col">
      <p>Now we are going to have an understanding of <em>kernel density estimation</em>. </p>
      <p>KDE (<em>kernel density estimation</em>) is used to estimate the unknown density function in probability theory.</p>
      <p>It is one of the non-parametric test methods, proposed by <em>Rosenblatt </em>(1955) and <em>Emanuel Parzen </em>(1962), also known as <em>Parzen Window</em>.</p>
  </div>
</div>

<h2 class="mt-5">Principles</h2>
<hr>
<div class="row">
<div class="col-md">
  <h3 class="mt-3">General Algorithm Introduction</h3>

  <p>
    <em>Kernel density estimation </em>(KDE) is a non-parametric method for estimating the probability density function. It is using a smooth peak function called kernel to fit the observed data points, thereby simulating the true probability distribution curve.
    \[ \widehat{f}_h\left( x \right) = \frac{1}{n}\sum_{i=1}^{n} K_h \left( x-x_i \right) = \frac{1}{nh}\sum_{i=1}^{n} K \left( \frac{x-x_i}{h} \right) \] 
    \(K\) is kernel function and \(h\) is a smoothing parameter, called bandwidth or window. \(K_h\) is scaled kernel function.
  </p>
  <p>
    <strong>1. Kernel function</strong><br>
    In theory, all smoothed peak functions can be used as the kernel function of KDE. As long as the area under the function curve is equal to 1 for the normalized situation.
    \[ \int_{N} K\left(x\right) dx = 1 \]
    Sigmoid function, Gaussian function and rectangle function will be showed below:
    \[ K_\left(Sigmoid\right) \left( x \right) = \frac{1}{1+e ^ \left(-x\right)} \]
    \[ K_\left(Gaussian\right) \left( x \right) = \frac{1}{\sqrt{2\pi}} e^\left( -\frac{1}{2} x^2 \right) \]
    \[K_\left(Rectangle\right) \left( x \right)= 
    \begin{cases} 
    \frac{1}{2},  & \mbox{if } \left|x\right|\le 1 \\
    0, & \mbox{otherwise } 
    \end{cases}\]
    
  </p>
  <img class="img-fluid mx-auto d-block" src="imgs/all.svg" alt="kernel functions">
  <p>
    <strong>2. Bandwidth</strong><br>
    The bandwidth reflects the overall flatness of the KDE curve, that is, the proportion of the observed data points in the formation of the KDE curve. The larger the bandwidth is, the smaller the proportion of observed data points in the resulting curve shape will be. The smaller the bandwidth is, the steeper the overall curve of KDE will be. 
    It turns out that the choosing the bandwidth is the most difficult step in creating a good kernel density estimate that captures the underlying distribution of the variable. Choosing the bandwidth is a complicated topic that is better addressed in a more advanced book or paper, but here are some useful guidelines:<br> 
    &nbsp\((1)\) A small \(h\) results in a small standard deviation, and the kernel places most of the probability on the datum. Use this when the sample size is large and the data are tightly packed.<br>
    &nbsp\((2)\) A large \(h\) results in a large standard deviation, and the kernel spreads more of the probability from the datum to its neighbouring values. Use this when the sample size is small and the data are sparse.<br>
    Generally use the size of \(MISE\) (mean integrated squared error) to measure the performance of \(h\).
    \[ MISE\left( h \right)=\frac{1}{n}\sum_{i=1}^{n} \left(  \widehat{f}_h\left( x_i \right) - f\left( x_i \right) \right)^2 \] 
  </p>
  
  <p>
    <strong>3. 2-D KDE</strong><br>
    The two-dimensional kernel density estimation uses the principle of kernel density estimation to plot the density of points on the two-dimensional plane. It is a method to express probability density distribution through pixels. <br> In principle, 2-D kernel density estimation
    is the expansion of 1-D situation. The space of dataset transfer from axis to a plane, so the each data point will have 2 dimention information. We need to choose another value format to describe the density. Consequently the value of pixels can be used to present the density.
    In 2-D KDE, we use the distance parameter \(dist\) to replace the single parameter in 1-D dimention.
    \[ \widehat{f}_h\left( x, y \right) = \frac{1}{nh^2}\sum_{i=1}^{n} K_0 \left( \frac{dist_i}{h} \right) \]
    The principle and the evaluation method of kernel function and bandwidth are same to those in 1-D situation. We provide an interactive module for this part. 
  </p>
  

</div>
</div>

<h2 class="mt-5">Hands-On</h2>
<hr>
<div class="container mw-900">
<p>
  In the <em>Data</em> tab, you can draw data points, and we also provide three data sets: 3 cluster, Smiley and Grid 15 clusters. <br>Random noise and random clusters dataset are also available.
  <br>
  In the <em>Kernel Density Estimation</em> tab, you can choose from four kernel functions, which are: <em>Epanechnikov</em>, <em>Gaussian</em>, <em>Silverman K2</em> and <em>Silverman K3</em>. The graph below shows the shape of each function.<br>
  Search radius can be set manually, but the default is 30.
</p>

  <div class="card shadow">
                <div class="card-header">
                    <nav>
                        <div class="nav nav-tabs nav-justified card-header-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-data-tab" data-toggle="tab" role="tab"
                               href="#nav-data">Data</a>
                            <a class="nav-item nav-link" id="nav-kfunc-tab" data-toggle="tab" role="tab"
                               href="#nav-kfunc">Kernel Density Estimation</a>

                        </div>
                    </nav>
                </div><!-- card-header -->
                <div class="card-body p-3">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-data" role="tabpanel">
                            <div class="row d-flex align-items-center">
                                <div class="col-md my-1">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="check-draw-points">
                                        <label class="custom-control-label" for="check-draw-points">Draw Data
                                            Points</label>
                                    </div>
                                </div>
                                <div class="col-md my-1">
                                    <button type="button" class="btn btn-sm btn-block btn-secondary dropdown-toggle"
                                            data-toggle="dropdown"><i class="mdi mdi-plus-circle mr-1"></i>Add Dataset
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <button class="dropdown-item" id="btn-data-clusters">3 Clusters</button>
                                        <button class="dropdown-item" id="btn-data-smiley">Smiley</button>
                                        <button class="dropdown-item" id="btn-data-grid">Grid 15 Clusters</button>
                                        <div class="dropdown-divider"></div>
                                        <button class="dropdown-item" id="btn-data-random">Random Noise</button>
                                        <button class="dropdown-item" id="btn-data-random-clusters">Random Clusters
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md my-1">
                                    <button type="button" id="btn-data-clear" class="btn btn-sm btn-block btn-warning">
                                        <i class="mdi mdi-delete mr-1"></i>Clear Points
                                    </button>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane fade" id="nav-kfunc" role="tabpanel">
                            <div class="card-deck">
                                <div class="card">
                                    <h6 class="card-header">Kernel function</h6>
                                    <div class="card-body p-3">


                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="sel-kfunc">Function</label>
                                            </div>
                                            <div>
                                                <select class="custom-select custom-select-sm" id="sel-kfunc"
                                                        onclick="cll(this.value)">
                                                    <option value="epanechnikovFunc">Epanechnikov</option>
                                                    <option value="gaussianFunc">Gaussian</option>
                                                    <option value="silvermank2Func">Silverman K2</option>
                                                    <option value="silvermank3Func">Silverman K3</option>
                                                </select>

                                                <div id="div1" style="display:none">
                                                    <img class="img-fluid mx-auto d-block" src="imgs/epanechnikov.svg"><br>
                                                </div>
                                                <div id="div2" style="display:none">
                                                    <img class="img-fluid mx-auto d-block" src="imgs/gaussian.svg"><br>
                                                </div>
                                                <div id="div3" style="display:none">
                                                    <img class="img-fluid mx-auto d-block" src="imgs/k2.svg"><br>
                                                </div>
                                                <div id="div4" style="display:none">
                                                    <img class="img-fluid mx-auto d-block" src="imgs/k3.svg"><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <script>
                                  function cll(value){
                                      if(value=="epanechnikovFunc"){
                                          eval("div1.style.display=\"\";");
                                          eval("div2.style.display=\"none\";");
                                          eval("div3.style.display=\"none\";");
                                          eval("div4.style.display=\"none\";");}
                                    else if(value=="gaussianFunc"){
                                          eval("div1.style.display=\"none\";");
                                          eval("div2.style.display=\"\";");
                                          eval("div3.style.display=\"none\";");
                                          eval("div4.style.display=\"none\";");}
                                    else if(value=="silvermank2Func"){
                                          eval("div1.style.display=\"none\";");
                                          eval("div2.style.display=\"none\";");
                                          eval("div3.style.display=\"\";");
                                          eval("div4.style.display=\"none\";");}
                                    else{
                                          eval("div1.style.display=\"none\";");
                                          eval("div2.style.display=\"none\";");
                                          eval("div3.style.display=\"none\";");
                                          eval("div4.style.display=\"\";");}
                                  }
                               </script>


                                <div class="card">
                                    <h6 class="card-header">Search radius</h6>
                                    <div class="card-body p-3">
                                        <form class="form-inline mb-2">
                                            <label class="mr-5" for="rg-eps">ε:<span
                                                    class="text-primary text-monospace ml-2"
                                                    id="label-eps"></span></label>
                                            <input type="range" class="custom-range" min="0" max="500" value="30"
                                                   id="rg-eps">
                                        </form>
                                        <button type="button" id="btn-kde-run-stop"
                                                class="btn btn-block btn-sm btn-secondary">
                                            <span class="kde-run"><i class="mdi mdi-play mr-1"></i>Run KDE</span>
                                            <span class="kde-stop"><i class="mdi mdi-loading mdi-spin mr-1"></i>Stop KDE</span>
                                        </button>
                                        <br>
                                        <label class="timing" for="timing">Computation time: <span class="text-primary text-monospace ml-2" id="timer"></span></label>
                                        
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- tab-content -->
                </div><!-- card-body -->
                <div id="drawing" class="card-img-bottom bg-light border-top"></div>
            </div><!-- card -->

    </div>




